## Conjuntos, Aplicaciones y funciones (2002)

[Conjuntos, Aplicaciones y funciones](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)

```plantuml
@startmindmap
header
13 de octubre del 2021
endheader
title Cuadro Sinóptico del resumen sobre Conjuntos, Aplicaciones y funciones.
legend right
  Elaborado por:
  Erik Leonel
  Santiago Jimenez
endlegend
<style>
mindmapDiagram {
  Linecolor green
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  RoundCorner 10
  }
  .inicio {
    BackgroundColor lightblue
  }
  .pink {
    BackgroundColor #F5CBA7
  }
 .yellow {
    BackgroundColor #F7F9F9
  }
 .greencute {
    BackgroundColor #A3E4D7
  }
.green2 {
    BackgroundColor lightgreen
 RoundCorner 10
  FontSize 18
  }
}
</style>
* **Conjuntos, Aplicaciones y funciones**<<greem>>
** Pensamiento matematicos
***_ Por ejemplo
**** Vocaciones de las matematicas
****_ Como
**** Razonar
***_ Sobre
**** Dominio de las ideas
**** Filosofia de las matematicas

** Conjunto y Elemento <<rose>>
***_ Son
**** Idea intuitiva
*****_ Tiene
****** Relacion de pertenencia
**** Dimitivo
***** No se define


** Teoria de conjuntos<<rose>>
***_ Rama de
**** Lógica matemática
*****_ Estudia
****** Las propiedades
*******_ de los conjuntos
****** Las relaciones
*******_ de los conjuntos

** Propiedades de conjuntos <<rose>>
***_ Generales 
**** Inclusion de conjuntos
**** Conjuntos elementales
*****_ Como
****** Mecanismos de razonamiento
******  Propiedades abstractas para los conjuntos
****_ **La mas importante**
***** Relacion de orden 
****** Elementos A al B
****** Elementos B al A

** Operaciones de conjuntos <<rose>>
***_ Por
**** Conjuntos completos
***_ Mediante
**** álgebra de conjuntos
*****_ Armados sobre
****** Los conjuntos elementales
*******_ Similares a 
******** Las operaciones con números naturales.
********* Teoria numeros 
********* Teoria de conjuntos

**** **Interseccion**
*****_ Son
****** Todos los elementos comunes de A y B.
**** **Union**
*****_ Son
******  Todos los elementos de A y de B.
**** **Complementacion**
*****_ Son
****** Todos los elementos que no pertenecen a A.

**** **Diferencia de conjuntos**
*****_ Son
****** Elementos de A y B que no son comunes.

**** **Producto**
*****_ Son
****** Contiene todos los pares ordenados 

** Conjunto referencial <<rose>>
***_ Llamado 
**** Conjuntos universal 
*****_ Abarca
****** Objetos simples
****** Números
****** Conjuntos de números
****** Conjuntos de conjuntos de números
***_ Similar al 
**** Marco de referencia 
***** Para formar conjuntos.

*** Conjuntos vacios
****_ Son
***** Conjuntos Logicos
****_ Son
***** Conjunto sin elementos
****_ Se representa
***** por la letra ∅.

** Representaciones <<rose>>
***_ Utilizado para 
**** Forma de mostrar
**** Sistemas
*****_ Como
****** Diagramas de Venn
****** Muestran colecciones de cosas
*******_ por
******** medio de líneas cerradas.
***_ Un
**** Tema de interés en matemáticas
***_ La
**** Lógica de clases 
***_ Un 
**** Razonamiento diagramático
*****_ Como ejemplo
****** El conjunto vacío




** Propiedades de numeros cardinales <<rose>>
***_ Relaciona
**** Dos conjuntos
*****_ Solo si 
****** Tienen el mismo número de elementos.
****_ Son
***** Formula de cardinales
***** Acotacion de cardinales
***** Serie de relaciones
*****_ Sirve para
****** Obtener
******* Mayores
******* Menores
*******  Niveles
 
left

** Aplicaciones y funciones <<rose>>
*** Historia del cambio
****_ Dada por
***** Filosofos
*** Procesos de cambio 

***_ Ejemplos
**** Desiciones de gobiernos
***** Indices de pobreza
**** Economia
***** Cambios de la moneda nacional
***** Inflacion
**** Biologia
***** Controlar bacterias
****_ Todo cambio
***** Aplicado a matematicas


** Transformacion <<rose>>
***_ Es
**** Convertir elementos
*****_ Se debe
****** Identificar
****** Toda función 
*******_ que mapea a 
******** un conjunto X en otro conjunto

** Transformacion de Transformacion <<rose>>
***_ Es
**** Composicion de aplicaciones
*****_ sobre
***** Aplicación sucesiva de dos o más funciones 
******_ Sobre 
******* Un mismo elemento 
****_  Se realiza
***** Aplicando dichas funciones


** Aplicacion <<rose>>
***_ Es
**** Un tipo de Transformacion
*****_ Que convierte
****** Un grupo de conjunto
******* A un unico elemento.
*** Cada uno 
**** De los transformados
*****_ Debe 
****** Tener una aplicación
****** **Ser único**

** **Aplicacion inyectiva** <<rose>>
***_ Son
**** Cuando los elementos son diferentes
*****_ También
****** Lo son sus imágenes.
** **Aplicacion subyectiva**
***_ Son
**** Cuando son iguales su codominio 
***** Y su recorrido o rango.
** **Aplicacion biyectiva**
***_ Son
**** Cuando son inyectiva 
**** Cuando son sobreyectiva
**** Los 2 al mismo tiempo.


** Imagen  <<rose>>
***_ Tambien conocidas como
**** Imagen inversa, 
**** Antiimagen  
***** De una aplicación
****** Aplicaciones
*******_ Como 
******** Topología 
******** Teoría de la medida.
******_ Que
******* a un conjunto 
********_ Le corresponde
********* otro conjunto.

**** Contraimagen 




** Transformacion de Conjuntos de numeros  <<rose>>
***_ Son
**** Apliaciones más faciles de ver
****_ Como 
***** Funcion 
***** Graficas de funciones
****** **Representacion**
******_ Se clasifican en
******* Constantes
******* Funciones polinomicas de primer grado
******* Funciones racionales
******* Funciones radicales
******* Funciones algebraicas a trozos
******* Funciones exponenciales
******* Funciones logarítmicas
******* Funciones cuadraticas
******** Parabolas 

@endmindmap
```
## Funciones

[Funciones](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)

```plantuml
@startmindmap
header
13 de octubre del 2021
endheader
title Cuadro Sinóptico del resumen sobre Funciones.
legend right
  Elaborado por:
  Erik Leonel
  Santiago Jimenez
endlegend
<style>
mindmapDiagram {
  Linecolor green
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  RoundCorner 10
  }
  .inicio {
    BackgroundColor lightblue
  }
  .pink {
    BackgroundColor #F5CBA7
  }
 .yellow {
    BackgroundColor #F7F9F9
  }
 .greencute {
    BackgroundColor #A3E4D7
  }
.green2 {
    BackgroundColor lightgreen
 RoundCorner 10
  FontSize 18
  }
}
</style>

* Funciones<<inicio>>
**_ Es un
*** Analisis matematicos
****_ Como
***** Aplicacion especial 
***** Conjunto de numero
******_ se le suele llamar
*******  **f(x) (función que depende de x).**
**_ Refleja
*** El pensamiento humano 
*** Reglas
****_ Para 
***** Comprender el entorno
***** Resolver problemas

**_ Describen
*** Fenómenos

**** Cotidianos 
*****_ Por ejemplo
****** Matemáticas
******* Comprar cosas en un mercado
**** Psicológicos
*****_ Por ejemplo
****** Mecanismos biológicos

**** Científicos
*****_ Por ejemplo
****** Controlar poblacion de bacterias

**** Temperatura
*****_ Por ejemplo
****** Clima de una ciudad por horas
**** Economia 
*****_ Por ejemplo
****** Cambio de la moneda local

*** Mediante
**** La variable dependiente

*** Para cada valor de x
**** Le corresponde un único valor de y.
*****_ **solamente hay un valor**


** Aplicaciones  <<rose>>
***_ Son
**** **Transformaciones**
***** De un conjunto
***** A otro conjunto


** Representaciones<<rose>>
***_ Sirven para 
**** Representar en un plano 
***** Graficas
***** Cartesiana
***** Imagen 

**_ Dependiendo del
*** Comportamientos de funciones
****_ Ejemplos 

***** Funciones crecientes
****** Aumentan las variables
*******_ Aumenta
******** Algebraicamente cuando aumenta.

***** Funciones decrecientes
****** Disminuye las variables
*******_ Disminuye
******** Algebraicamente cuando disminuye.

***** Maximos Relativos
******_ Se refiere a 
******* valor mayor que toma 
********_ una función
*********  En un determinado intervalo.


***** Minimos Relavitos
******_ Se refiere a 
******* valor menor que toma 
********_ una función
*********  En un determinado intervalo.


***** Limites
****** Es el valor al que tiende la función
*******_ Cuando la variable independiente
******** Tiende a un valor dado.


** Funciones sencillas<<rose>>
***_ Operaciones para 
**** Obtener sus valores
*****_ Ejemplo
****** Polinómicas
****** Racionales


** Funciones complejas<<rose>>
*** Mezcla de funciones sencillas
****_ Como 
***** Derivadas
******_ Sirve para 
******* Describir la realidad
******* Aspectos más complicados
******_ Resuelve 
******* Problema del aproximacion compleja
********_ Mediante una 
********* Dfuncion simple

** Usos<<rose>>
***_ Se pueden calcular 
**** Para cada funcion
*****_ Velocidad 


** Son<<rose>>
***_ Ideas basicas
**** De las matematicas
*****_ Se pueden llegar 
****** A temas muy profundos.

@endmindmap
```
## Las matemáticas de los computadores.
[La matemática del computador. ](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)
[Las matemáticas de los computadores. ](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)
Eduardo Ramos Méndez catedrático de Estadística e Investigación Operativa , UNED
Isabel Baeza Fernández redactora - locutora, CEMAV, UNED

```plantuml
@startmindmap
header
13 de octubre del 2021
endheader
title Cuadro Sinóptico del resumen sobre Las matemáticas de los computadores.
legend right
  Elaborado por:
  Erik Leonel
  Santiago Jimenez
endlegend
<style>
mindmapDiagram {
  Linecolor green
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  RoundCorner 10
  }
  .inicio {
    BackgroundColor lightblue
  }
  .pink {
    BackgroundColor #F5CBA7
  }
 .yellow {
    BackgroundColor #F7F9F9
  }
 .greencute {
    BackgroundColor #A3E4D7
  }
.green2 {
    BackgroundColor lightgreen
 RoundCorner 10
  FontSize 18
  }
}
</style>

* Las matemáticas de los computadores<<inicio>>

** Computador<<green>>
***_ Es
**** **Maquina que soluciona problemas**
*****_ Generalmente
****** Matematicos
***_ Utiliza 
**** Sistema binario <<rose>>
*****_ Es un
****** Sistema simple

*******_ Basada en 
******** Sistema booleano
******** Representaciones de 0 y 1
******_ Puede representar
******* Imagenes
******* Videos
******* Archivos

**** Paso de corriente Electrica <<rose>>
*****_ Para cambiar
****** Posiciones de memoria
****** Numero entero
****** Punto flotante

** Aritmetica del computador<<green>>

***_ Es
**** Calculo con numeros abstractos<<rose>>

*****_ Como 
****** **La aproximacion de un numero** 
******* Raiz de 2
******* Pi
******* Numero con punto flotante

** Tecnica de truncamiento<<green>>

***_ Consiste en
**** Cortar el número exacto sin preocuparnos de cómo continúa
*****_ Por ejemplo
****** 17,34567
*******_ a
******** 17,34
****** 2,91276
*******_ a
******** 2,91


** Tecnica de redondeo<<green>>

***_ Consiste en 
**** No considerar los decimales
***** Cortando el número para quedarse sólo con el entero.
******_ Por ejemplo
******* 8,1463
********_ a
********* 8,15.

** Representaciones de numeros<<green>>

***_ Combinación de signos
**** Letras o números <<rose>>

*****_ Para 
******  Identificación de cantidades correspondientes
******* Determinadas unidades de medida
********_ Como
********* Mantiza
********* Numeros enteros

**_ Utiliza la<<green>>

*** Aritmetica binaria <<rose>>
****_ Son 
***** Operaciones elementales
******_ **Por la en la ALU**
******* Sistema base 2
********_ Son la
********* Adición
*********  Sustracción
*********  Multiplicación
*********  División.

@endmindmap
```